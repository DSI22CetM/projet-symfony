<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Admin;
use App\Form\AdminType;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
    	$admin = new Admin();
    	$form = $this->createForm(AdminType::class, $admin);
        return $this->render('admin/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
