<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use App\Form\UserType;

class UserController extends AbstractController
{

	/**
     * @Route("/create/account", name="create_account")
     */
    public function new(Request $request): Response
    {
        $user = new User();
        $em = $this->getDoctrine()->getManager();
    	$form = $this->createForm(UserType::class);
        $form->handleRequest($request);
        $name = $form['name']->getData();
        $surname = $form['surname']->getData();
        $email = $form['email']->getData();
        $password = $form['password']->getData();
        $user->setName($name);
        $user->setSurname($surname);
        $user->setEmail($email);
        $user->setPassword($password);
        if($form->isSubmitted() && $form->isValid()){
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('index');
        }
        return $this->render('user/index.html.twig', [
            'form'=> $form->createView()
        ]);
    }
}
